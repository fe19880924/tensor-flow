import tensorflow as tf

# session
matrix1 = tf.constant([[3, 3]])
matrix2 = tf.constant([[2], [2]])

product = tf.matmul(matrix1, matrix2)

sess0 = tf.Session()
result = sess0.run(product)
sess0.close()
print(result)

with tf.Session() as sess:
    result2 = sess.run(product)
    print(result2)

# variable
state = tf.Variable(0, name='counter')
one = tf.constant(1)

new_value = tf.add(state, one)
update = tf.assign(state, new_value)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    print(sess.run(new_value))
    for _ in range(3):
        sess.run(update)
        print({'state': sess.run(state), 'new_value': sess.run(new_value), 'update': sess.run(update)})

# placeholder
input1 = tf.placeholder(tf.float32)
input2 = tf.placeholder(tf.float32)

output = tf.multiply(input1, input2)
with tf.Session() as sess:
    print(sess.run(output, feed_dict={input1: [7.], input2: [2.]}))









